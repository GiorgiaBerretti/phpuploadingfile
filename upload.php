<?php
if(isset($_POST['submit'])){
   $file = $_FILE['fileToUpload'];
   
   $fileName = $_FILES['fileToUpload']['name'];
   $fileTmp = $_FILES['fileToUpload']['tmp_name'];
   $fileSize = $_FILES['fileToUpload']['size'];
   $fileError = $_FILES['fileToUpload']['error'];
   $fileType = $_FILES['fileToUpload']['type'];

   $fileExt = explode ('.', $fileName);
   $fileActualExt = strtolower(end($fileExt));

   $allowed = array('jpg', 'jpeg', 'png', 'pdf');
   if(in_array($fileActualExt, $allowed)){
    if($fileError === 0){
      if($fileSize < 1000000){
        $fileNameNew = uniqid('', true).".".$fileActualExt;
        $fileDestination = 'uploads/'.$fileNameNew;
        move_uploaded_file($fileTmp, $fileDestination);
        $message = urlencode("File caricato con successo!");
        header("Location: index.php?Message=".$message);
        die;
      } else {
        echo "Il file inserito è troppo grande.";
      }
    } else {
      echo "C'è stato un errore in fase di caricamento file!";
    }
   } else {
    echo "L'estensione del file inserito non è consentita.";
   }


}

?>