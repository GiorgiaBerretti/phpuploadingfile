<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <!-- bootstrap css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <!-- my css -->
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  <div class="alert alert-success" role="alert">
      <?php
      if(isset($_GET['Message'])){
        echo $_GET['Message'];
      }
      ?>
  </div>
    <header>
        <div class="container mt-5">
            <div class="row h1Row justify-content-center align-items-center">
                <div class="col-12 text-center">
                    <h1>Prova caricamento file php</h1>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container mainContainer">
            <div class="row h-100 align-items-center">
                <form enctype="multipart/form-data" action="upload.php" method="POST">
                    <div class="row align-items-center">
                        <div class="col-10">
                            <input type="file" id="fileToUpload" name="fileToUpload" class="form-control">
                        </div>
                        <div class="col-2">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
    <footer>
        <div class="container-fluid footer">
            <div class="row h-100 align-items-center">
                <div class="col text-center ">
                    <p class="fst-italic">Caricamento file in database tramite l'utilizzo esclusivo di php</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- bootstrap js -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>

<!-- per far partire il programma nel browser è necessario far partire il server locale:

php -S localhost:8000 -->